(function ($, Drupal) {
  let scriptHasRunOnce = false;

  setTimeout(function () {
    let messageText = 'To view this content, you need to';
    let messageLinkText = 'accept the following cookies:';
    let preferenceLabel = 'Preferences';
    let statisticsLabel = 'Statistics';
    let marketingLabel = 'Marketing';

    Drupal.behaviors.cookieConsentNotice = {
      attach(context, settings) {
        if (
          typeof drupalSettings.cookieConsentNotice.messageText !==
            'undefined' &&
          drupalSettings.cookieConsentNotice.messageText !== null
        ) {
          messageText = drupalSettings.cookieConsentNotice.messageText;
        }
        if (
          typeof drupalSettings.cookieConsentNotice.messageLinkText !==
            'undefined' &&
          drupalSettings.cookieConsentNotice.messageLinkText !== null
        ) {
          messageLinkText = drupalSettings.cookieConsentNotice.messageLinkText;
        }
        if (
          typeof drupalSettings.cookieConsentNotice.preferenceLabel !==
            'undefined' &&
          drupalSettings.cookieConsentNotice.preferenceLabel !== null
        ) {
          preferenceLabel = drupalSettings.cookieConsentNotice.preferenceLabel;
        }
        if (
          typeof drupalSettings.cookieConsentNotice.statisticsLabel !==
            'undefined' &&
          drupalSettings.cookieConsentNotice.statisticsLabel !== null
        ) {
          statisticsLabel = drupalSettings.cookieConsentNotice.statisticsLabel;
        }
        if (
          typeof drupalSettings.cookieConsentNotice.marketingLabel !==
            'undefined' &&
          drupalSettings.cookieConsentNotice.marketingLabel !== null
        ) {
          marketingLabel = drupalSettings.cookieConsentNotice.marketingLabel;
        }

        if (!scriptHasRunOnce) {
          scriptHasRunOnce = true;

          const newDiv = document.createElement('div');

          let parentElement = '';
          let cookiesListItems = '';
          let cookiesClassItems = '';
          let cookieClass = '';

          if ($('.cookieconsent-optin-preferences', context).length) {
            const translatablePreferences = Drupal.t(preferenceLabel);
            cookiesListItems += `<li style="display: list-item;" class="cookieconsent-optout-preferences">${translatablePreferences}</li>`;
            if (
              jQuery('.cookieconsent-optin-preferences').css('display') !==
              'none'
            ) {
              cookiesClassItems = 'cookieconsent-optout-preferences';
            }
            parentElement = $('.cookieconsent-optin-preferences').parent();
            cookieClass = '.cookieconsent-optin-preferences';
          }
          if ($('.cookieconsent-optin-statistics', context).length) {
            const translatableStatistics = Drupal.t(statisticsLabel);
            cookiesListItems += `<li style="display: list-item;" class="cookieconsent-optout-statistics">${translatableStatistics}</li>`;
            if (
              jQuery('.cookieconsent-optin-statistics').css('display') !==
              'none'
            ) {
              cookiesClassItems = 'cookieconsent-optout-statistics';
            }
            parentElement = $('.cookieconsent-optin-statistics').parent();
            cookieClass = '.cookieconsent-optin-statistics';
          }
          if ($('.cookieconsent-optin-marketing', context).length) {
            const translatableMarketing = Drupal.t(marketingLabel);
            cookiesListItems += `<li style="display: list-item;" class="cookieconsent-optout-marketing">${translatableMarketing}</li>`;
            if (
              jQuery('.cookieconsent-optin-marketing').css('display') !== 'none'
            ) {
              cookiesClassItems = 'cookieconsent-optout-marketing';
            }
            parentElement = $('.cookieconsent-optin-marketing').parent();
            cookieClass = '.cookieconsent-optin-marketing';
          }

          newDiv.innerHTML = `${Drupal.t(
            `${messageText} <a href="#" onclick="CookieConsent.renew()">${messageLinkText}</a>`,
          )}<ul>${cookiesListItems}</ul>`;

          newDiv.className = `cookie-consent-div ${cookiesClassItems}`;

          // only append the new div if the parent element exists
          if (parentElement !== '') {
            parentElement.append(newDiv);
          }

          // repeatedly check if the iframe is being displayed, if it is then
          // remove the cookies message div before the user has set any cookies
          // the display sometimes needs a bit of help...
          const displayCheckInterval = setInterval(function () {
            if (document.querySelector(cookieClass)) {
              const iframeDisplayStyle = getComputedStyle(
                document.querySelector(cookieClass),
              ).display;

              if (iframeDisplayStyle !== 'none') {
                document.querySelector('.cookie-consent-div').style.display =
                  'none';

                // kill the interval to prevent unnecessary processing overhead
                clearInterval(displayCheckInterval);
              }
            }
          }, 500);
        }
      },
    };

    const domReady = (callback) => {
      const listener = () => {
        callback();
        document.removeEventListener('DOMContentLoaded', listener);
      };
      if (document.readyState !== 'loading') {
        setTimeout(callback, 0);
      } else {
        document.addEventListener('DOMContentLoaded', listener);
      }
    };

    domReady(() => {
      Drupal.attachBehaviors(document);
    });
  }, 1000);
})(jQuery, Drupal);
