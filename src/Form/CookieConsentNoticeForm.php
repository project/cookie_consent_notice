<?php

namespace Drupal\cookie_consent_notice\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to allow users to configure the notice settings and content.
 */
class CookieConsentNoticeForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cookie_consent_notice.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookie_consent_notice_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cookie_consent_notice.settings');

    $config_check = $config->getRawData();

    if (count($config_check) !== 0) {
      $message_text = $config->get('message_text');
      $message_link_text = $config->get('message_link_text');
      $preference_label = $config->get('preference_label');
      $statistics_label = $config->get('statistics_label');
      $marketing_label = $config->get('marketing_label');
    }
    else {
      $message_text = TRUE;
      $message_link_text = TRUE;
      $preference_label = TRUE;
      $statistics_label = TRUE;
      $marketing_label = TRUE;
    }

    $message_text = $config->get('message_text');
    $form['message_text'] = [
      '#default_value' => $message_text,
      '#placeholder' => $this->t('To view this content, you need to'),
      '#description' => $this->t('The message you would like to display to the user EXCLUDING the link text to indicate which cookies are required for the current content.'),
      '#maxlength' => 255,
      '#size' => 80,
      '#title' => $this->t('Message Text'),
      '#type' => 'textfield',
    ];

    $message_link_text = $config->get('message_link_text');
    $form['message_link_text'] = [
      '#default_value' => $message_link_text,
      '#placeholder' => $this->t('accept the following cookies:'),
      '#description' => $this->t('The message text continued, ONLY the link text.'),
      '#maxlength' => 255,
      '#size' => 80,
      '#title' => $this->t('Message Link Text'),
      '#type' => 'textfield',
    ];

    $preference_label = $config->get('preference_label');
    $form['preference_label'] = [
      '#default_value' => $preference_label,
      '#placeholder' => $this->t('Preferences'),
      '#description' => $this->t('The custom label you would like to assign to the preferences cookies text in the notice.'),
      '#maxlength' => 255,
      '#size' => 80,
      '#title' => $this->t('Preferences Custom Label'),
      '#type' => 'textfield',
    ];

    $statistics_label = $config->get('statistics_label');
    $form['statistics_label'] = [
      '#default_value' => $statistics_label,
      '#placeholder' => $this->t('Statistics'),
      '#description' => $this->t('The custom label you would like to assign to the statistics cookies text in the notice.'),
      '#maxlength' => 255,
      '#size' => 80,
      '#title' => $this->t('Statistics Custom Label'),
      '#type' => 'textfield',
    ];

    $marketing_label = $config->get('marketing_label');
    $form['marketing_label'] = [
      '#default_value' => $marketing_label,
      '#placeholder' => $this->t('Marketing'),
      '#description' => $this->t('The custom label you would like to assign to the marketing cookies text in the notice.'),
      '#maxlength' => 255,
      '#size' => 80,
      '#title' => $this->t('Marketing Custom Label'),
      '#type' => 'textfield',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cookie_consent_notice.settings')
      ->set('message_text', $form_state->getValue('message_text'))
      ->set('message_link_text', $form_state->getValue('message_link_text'))
      ->set('preference_label', $form_state->getValue('preference_label'))
      ->set('statistics_label', $form_state->getValue('statistics_label'))
      ->set('marketing_label', $form_state->getValue('marketing_label'))
      ->save();
  }

}
